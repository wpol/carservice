package com.wojtek.CarService.repositories;

import com.wojtek.CarService.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);
}

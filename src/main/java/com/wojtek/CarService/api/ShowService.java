package com.wojtek.CarService.api;

import com.wojtek.CarService.entities.Service;

public class ShowService {
    public String kind;
    public String type;
    public String name;
    public Double price;

    public ShowService(Service service) {
        this.kind = service.kind();
        this.type = service.type();
        this.name = service.name();
        this.price = service.price();
    }
}
